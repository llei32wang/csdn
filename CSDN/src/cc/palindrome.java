package cc;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class palindrome {

	public static void main(String[] args) {
		System.out.println(palindrome1("aaabccbaaa"));
		palindrome2("aaabc");
	}
	//验证
	public static void palindrome2(String s){
		HashMap<String,String> map=new HashMap<String,String>();
		int i=0;
		palindrome2(s.toCharArray(),map,i);
		Set<Entry<String,String>> set=map.entrySet();
		Iterator<Entry<String,String>> it=set.iterator();
		int n=0;
		while(it.hasNext()){
			++n;
			System.out.println(n+" "+it.next().getValue());
		}	
	}
	//验证
	public static void palindrome2(char[] cs,HashMap<String,String> map,int i){
		if(i<cs.length){
			String s;
			char[] cs1=null;
			char c=0;		
			int t=0;
			for(int j=i;j<cs.length;++j ){
				t=i;
				cs1=cd(cs);
				c=cs1[i];
			    cs1[i]=cs1[j];
			    cs1[j]=c;	
	 		    s=String.valueOf(cs1);
				if(map.get(s)==null)
					map.put(s, s);  
			   palindrome2(cs1,map,++t);
			}   
		}		
	}
	//复制数组
	public static char[] cd(char[] c){
		char[] a=new char[c.length];
		for(int i=0;i<c.length;i++)
			a[i]=c[i];
		return a;
	}
	//计算
	public static int palindrome1(String s){
		int len=s.length();
		HashMap<Character,Integer> map=new HashMap<Character,Integer>();
		for(char c:s.toCharArray()){
		    if(map.get(c)==null){
		    	map.put(c, 1);
		    }else{
		    	map.put(c,(int)map.get(c)+1);
		    }    	
		}
		int odd=0;
		int tmp=0;
		Set<Entry<Character,Integer>> set=map.entrySet();
		Iterator<Entry<Character,Integer>> it=set.iterator();
		while(it.hasNext()){
			tmp=it.next().getValue();
			if(tmp%2!=0)
				odd++;
			if(odd>1)
				return 0;		
		}
		if(len%2==0){
		   if(odd>0)
			   return 0;
		   else{
			   return cc(len,set); 
		   }		   
		}else{
			if(odd==0)
				return 0;
			else{
			    return cc(len-1,set); 
			}				
		}
	}
	
	
	private static int cc(int len,Set<Entry<Character,Integer>> set){
		Iterator<Entry<Character,Integer>> it=set.iterator();
		len=factorial(len/2);
		while(it.hasNext()){
		   len=len/factorial(it.next().getValue()/2);
		}
		return len;
	}
	
	//阶乘
	private static int factorial(int n){
		if(n>1)
			return n*factorial(n-1);
		else
			return 1;
	}

}
